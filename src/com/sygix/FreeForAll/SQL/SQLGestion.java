/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.SQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.sygix.FreeForAll.FreeForAll;

public class SQLGestion {
	
	static Connection con = null;

    public static void connect() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url_ = FreeForAll.config.get("SQL.URL").toString();
        String pass = FreeForAll.config.get("SQL.Pass").toString();
        String user = FreeForAll.config.get("SQL.User").toString();

        con = DriverManager.getConnection(url_, user, pass);
    }
    
    public static void disconnect() throws SQLException {
    	if(con != null){
    		con.close();
    	}
    }
    
    @SuppressWarnings("unused")
    public static void registerPlugin() throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        try {
            ResultSet res = state.executeQuery("SELECT * FROM players");
        } catch(SQLException e) {
            state.execute("CREATE TABLE players ( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, uid VARCHAR(100), pseudo VARCHAR(100), time LONG, lastconnect LONG, coins DOUBLE, halo DOUBLE, date_buy_halo VARCHAR(100), parrain VARCHAR(100))");
        }
        try{
        	ResultSet res = state.executeQuery("SELECT * FROM servers");
        }catch(SQLException e){
        	state.execute("CREATE TABLE servers ( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, serveur VARCHAR(100), state VARCHAR(100), joueurs INT, maintenance BOOLEAN)");
        }
        try{
        	ResultSet res = state.executeQuery("SELECT * FROM ffa");
        }catch(SQLException e){
        	state.execute("CREATE TABLE ffa ( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, uid VARCHAR(64), cle VARCHAR(256), value VARCHAR(256))");
        }
        state.close();
    }
    
    public static boolean registerOnBDD(Player p) throws SQLException, IOException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='" + p.getUniqueId()+ "'");
        while(!res.next()){
        	Statement s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            s.executeUpdate("INSERT INTO players (id, uid, pseudo, time, lastconnect, coins, halo, date_buy_halo) VALUES (null, '"+p.getUniqueId()+"', '"+p.getName()+"', 0, 0, 0.0, 1.0, null)");
            state.close();
            return false;
        }
        state.close();
        return true;
    }
    
    public static double getHalo(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
        	double halo = res.getDouble("halo");
            state.close();
            return halo;
        }
        state.close();
        return 0.0;
    }
    
    public static double getHalo(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
        	double halo = res.getDouble("halo");
            state.close();
            return halo;
        }
        state.close();
        return 0.0;
    }
    
    public static void setCoins(Player p, double coins) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET coins="+coins+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static double getCoins(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
        	double coins = res.getDouble("coins");
            state.close();
            return coins;
        }
        state.close();
        return 0.0;
    }
    
    public static double getCoins(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
        	double coins = res.getDouble("coins");
            state.close();
            return coins;
        }
        state.close();
        return 0.0;
    }
    
    //serveurs
    
    public static boolean registerServerOnBDD(String server) throws SQLException, IOException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='" + server + "'");
        while(!res.next()){
        	Statement s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            s.executeUpdate("INSERT INTO servers (id, serveur, state, joueurs, maintenance) VALUES (null, '"+server+"', null, 0, 0)");
            state.close();
            return false;
        }
        state.close();
        return true;
    }
    
    public static void setState(String serveur, String State) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE servers SET state='"+State+"' WHERE serveur='"+serveur+"'");
        state.close();
    }
    
    public static String getState(String serveur) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='"+serveur+"'");
        while(res.next()) {
            String State = res.getString("state");
            state.close();
            return State;
        }
        state.close();
        return null;
    }
    
    public static void setJoueurs(String serveur, int nbr) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE servers SET joueurs="+nbr+" WHERE serveur='"+serveur+"'");
        state.close();
    }
    
    public static int getJoueurs(String serveur) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='"+serveur+"'");
        while(res.next()) {
            int nbr = res.getInt("joueurs");
            state.close();
            return nbr;
        }
        state.close();
        return 1812;
    }
    
    //FFA
    
    public static void registerOnBDDFFA(String uid) throws SQLException, IOException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM ffa WHERE uid='"+uid+"'");
        while(!res.next()){
        	Statement s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            s.executeUpdate("INSERT INTO ffa (id, uid, cle, value) VALUES (null, '"+uid+"', 'rank', 'D�butant')");
            s.executeUpdate("INSERT INTO ffa (id, uid, cle, value) VALUES (null, '"+uid+"', 'kit', 'PvP')");
            s.executeUpdate("INSERT INTO ffa (id, uid, cle, value) VALUES (null, '"+uid+"', 'kills', '0')");
            s.executeUpdate("INSERT INTO ffa (id, uid, cle, value) VALUES (null, '"+uid+"', 'morts', '0')");
            s.executeUpdate("INSERT INTO ffa (id, uid, cle, value) VALUES (null, '"+uid+"', 'xp', '0')");
            state.close();
            return;
        }
        state.close();
        return;
    }
    
    public static List<String> getKits(String uid) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM ffa WHERE uid='"+uid+"' AND cle='kit'");
        List<String> list = new ArrayList<String>();
        while(res.next()) {
        	list.add(res.getString("value"));
        }
        state.close();
        return list;
    }
    
    public static boolean checkKit(String uid, String kitname) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM ffa WHERE uid='"+uid+"' AND cle='kit' AND value='"+kitname+"'");
        while(!res.next()) {
            state.close();
            return false;
        }
        state.close();
        return true;
    }
    
    public static String getValue(String uid, String key) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM ffa WHERE uid='"+uid+"' AND cle='"+key+"'");
        String rank = null;
        while(res.next()) {
        	rank = res.getString("value");
        }
        state.close();
        return rank;
    }
    
    public static void setValue(String uid, String key, String value) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE ffa SET value='"+value+"' WHERE uid='"+uid+"' AND cle='"+key+"'");
        state.close();
    }
    
    public static void addKey(String uid, String key, String value) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("INSERT INTO ffa (id, uid, cle, value) VALUES (null, '"+uid+"', '"+key+"', '"+value+"')");
        state.close();
    }
    
    public static void removeKey(String uid, String key, String value) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("DELETE FROM ffa WHERE uid='"+uid+"' AND cle='"+key+"' AND value='"+value+"'");
        state.close();
    }

}
