/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events.Player;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Runnables.PlayerToArena;

public class InventoryClick implements Listener  {
	
	@EventHandler
	public void onPlayerInventoryClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getInventory();
		try{
			if(inv.getName().equalsIgnoreCase(ChatColor.DARK_RED+""+ChatColor.BOLD+"Sélectionne ton kit")){
		        String kitname = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName().toLowerCase());
	            PlayerToArena.add(p, FreeForAll.playerArena.get(p), kitname);
			}else{
				e.setCancelled(true);
			}
		}catch(Exception e1){}
	}

}
