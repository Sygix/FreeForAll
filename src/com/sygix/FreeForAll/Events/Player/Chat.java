/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events.Player;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Utils.BroadcastMessage;
import com.sygix.FreeForAll.Utils.GamePrefix;

@SuppressWarnings("deprecation")
public class Chat implements Listener {
	
	@EventHandler
	public void onPlayerChat(PlayerChatEvent e){
		Player p = e.getPlayer();
		String m = e.getMessage();
		if((m.contains("�") || m.contains("&")) && (p.hasPermission("DirektServ.helper") || p.hasPermission("DirektServ.architecte") || p.hasPermission("DirektServ.developpeur"))){
			m = ChatColor.translateAlternateColorCodes('�', m);
			m = ChatColor.translateAlternateColorCodes('&', m);
		}
		e.setCancelled(true);
		if(!Join.loadingPlayers.contains(p)){
			if(FreeForAll.playerRank.get(p) != null){
				String rank = FreeForAll.playerRank.get(p);
				BroadcastMessage.broadcast(ChatColor.translateAlternateColorCodes('&', FreeForAll.config.getString("Rank."+rank+".Color"))+"~"+rank+"~�7 "+p.getDisplayName()+" : "+m);
			}else{
				p.sendMessage(GamePrefix.getGamePrefix()+"�cVos donn�es sont \"null\", veuillez vous reconnecter.");
			}
		}else{
			p.sendMessage(GamePrefix.getGamePrefix()+"�cVos donn�es sont en cours de chargement.");
		}
	}

}
