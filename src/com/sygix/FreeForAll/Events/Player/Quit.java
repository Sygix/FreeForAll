/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events.Player;

import java.sql.SQLException;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Runnables.PlayerToArena;
import com.sygix.FreeForAll.SQL.SQLGestion;

public class Quit implements Listener {
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		e.setQuitMessage("");
		if(FreeForAll.playerArena.containsKey(p)){
			PlayerToArena.remove(p);
		}
		if(Join.loadingPlayers.contains(p))return;
		if(!FreeForAll.playerRank.containsKey(p))return;
		try {
			SQLGestion.setValue(p.getUniqueId().toString(), "kills", FreeForAll.playerKills.get(p).toString());
			SQLGestion.setValue(p.getUniqueId().toString(), "morts", FreeForAll.playerMorts.get(p).toString());
			SQLGestion.setValue(p.getUniqueId().toString(), "rank", FreeForAll.playerRank.get(p));
			SQLGestion.setValue(p.getUniqueId().toString(), "xp", FreeForAll.playerXP.get(p).toString());
			SQLGestion.setCoins(p, FreeForAll.playerDC.get(p));
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

}
