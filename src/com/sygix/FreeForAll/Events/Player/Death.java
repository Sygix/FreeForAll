/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events.Player;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Runnables.PlayerToArena;
import com.sygix.FreeForAll.Utils.GamePrefix;
import com.sygix.FreeForAll.Utils.KillStreak;
import com.sygix.FreeForAll.Utils.PlayerAutoRespawnEvent;
import com.sygix.FreeForAll.Utils.PlayerPreAutoRespawnEvent;

public class Death implements Listener {
	
	@EventHandler
	public void onPlayerDeath(final PlayerDeathEvent e) {

		final Location deathLoc = e.getEntity().getLocation();

		final Player player = e.getEntity();

		PlayerPreAutoRespawnEvent ppare = new PlayerPreAutoRespawnEvent(player, deathLoc);

		Bukkit.getPluginManager().callEvent(ppare);

		if (ppare.isCancelled())
			return;

		Bukkit.getScheduler().scheduleSyncDelayedTask(FreeForAll.getInstance(), new Runnable() {
			@Override
			public void run() {
				player.spigot().respawn();
				Configuration c = FreeForAll.config;
				String arena = FreeForAll.playerArena.get(player);
				Location respawnLoc = new Location(Bukkit.getServer().getWorld(c.getString("Arena."+arena+".World")), c.getInt("Arena."+arena+".Spawn.X"), c.getInt("Arena."+arena+".Spawn.Y"), c.getInt("Arena."+arena+".Spawn.Z"));
				Bukkit.getPluginManager().callEvent(new PlayerAutoRespawnEvent(e.getEntity(), deathLoc, respawnLoc));
			}
		}, 1L);
		
		e.setKeepInventory(true);
		e.setKeepLevel(true);
		e.setDeathMessage(GamePrefix.getGamePrefix()+"�3"+e.getDeathMessage());
		
		 //ADD KILL
		if(player.getKiller() != null){
			Player killer = player.getKiller();
			FreeForAll.playerKills.replace(killer, FreeForAll.playerKills.get(killer)+1);
			KillStreak.addKillStreak(killer); //Coins,xp et message dans le Killstreak
		}
		
		//ADD MORT
		FreeForAll.playerMorts.replace(player, FreeForAll.playerMorts.get(player)+1);
		KillStreak.resetKillStreak(player); //Coins,xp et message dans le Killstreak
	}
	
	@EventHandler
	public void onPlayerAutoRespawn(PlayerAutoRespawnEvent e){
		Player p = e.getPlayer();
		p.teleport(e.getRespawnLocation());
		PlayerToArena.giveKits(p, FreeForAll.playerKit.get(p));
		p.setFoodLevel(19);
	}

}
