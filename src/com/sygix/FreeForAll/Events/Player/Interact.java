/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events.Player;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Utils.InventoryManager;

public class Interact implements Listener {
	
	String Arena;
	
	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent e){
		e.setCancelled(true);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		Action a = e.getAction();
		
		try{
			if((a == Action.RIGHT_CLICK_AIR) || (a == Action.RIGHT_CLICK_BLOCK)){
				if(a == Action.RIGHT_CLICK_BLOCK){
					if (e.getClickedBlock().getState() instanceof Sign && e.getClickedBlock() != null){
						Sign sign = (Sign) e.getClickedBlock().getState();
						if(sign.getLine(0).contains("[FreeForAll]")){
							InventoryManager.selectKitMenu(p);
							FreeForAll.playerArena.put(p, sign.getLine(1));
							e.setCancelled(true);
						}
					}
				}
				
				if(p.getItemInHand() != null){
					if(p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Boutique de kits "+ChatColor.GRAY+"(Clique droit)")){
						p.sendMessage("�cSoon");
						e.setCancelled(true);
						return;
					}else if(p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Mes kits "+ChatColor.GRAY+"(Clique droit)")){
						p.sendMessage("�cSoon");
						e.setCancelled(true);
						return;
					}else if(p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GOLD+""+ChatColor.BOLD+"Acheter un kit random "+ChatColor.GRAY+"(Clique droit)")){
						p.sendMessage("�cSoon");
						e.setCancelled(true);
						return;
					}else e.setCancelled(true);
				}
			}
		
			if(e.getAction().equals(Action.PHYSICAL)){			
				e.setCancelled(true);
			}
		}catch(Exception e2){}
	}

}
