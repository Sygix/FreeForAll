/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events.Player;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Runnables.Kits;
import com.sygix.FreeForAll.Utils.GamePrefix;

public class Consume implements Listener{
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerConsumeEvent(final PlayerInteractEvent e){
		final Player p = e.getPlayer();
		final ItemStack i = e.getItem();
		try{
			if(i.getType().equals(Material.MUSHROOM_SOUP)){
				double maxlive = FreeForAll.config.getDouble("Kits."+Kits.getKitConfigName(FreeForAll.playerKit.get(p))+".Max-Health");
				e.setCancelled(true);
				if(maxlive == p.getHealth()){
					p.sendMessage(GamePrefix.getGamePrefix()+"�cVous avez toute votre vie.");
				}else if(maxlive > p.getHealth()){
					if(maxlive-p.getHealth() < 2){
						p.setHealth(maxlive);
					}
					p.setHealth(p.getHealth()+2);
				}
			}
			if(i.getType().equals(Material.POTION)){
				Bukkit.getScheduler().runTaskLaterAsynchronously(FreeForAll.getInstance(), new BukkitRunnable(){

					@Override
					public void run() {
						p.getInventory().remove(Material.GLASS_BOTTLE);
						p.getInventory().addItem(i);
					}
					
				}, 20 * 4);
			}
		}catch(Exception e1){}
	}

}
