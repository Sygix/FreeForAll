/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events.Player;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Runnables.Kits;
import com.sygix.FreeForAll.Runnables.PlayerToArena;
import com.sygix.FreeForAll.SQL.SQLGestion;
import com.sygix.FreeForAll.Utils.GamePrefix;
import com.sygix.FreeForAll.Utils.scoreboard;

public class Join implements Listener {
	
	public static ArrayList<Player> loadingPlayers= new ArrayList<Player>();
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		e.setJoinMessage("");
		p.setFoodLevel(20);
		p.setGameMode(GameMode.ADVENTURE);
		PlayerToArena.give(p);
		p.teleport(Bukkit.getServer().getWorlds().get(0).getSpawnLocation());
		p.sendMessage(GamePrefix.getGamePrefix()+"�cChargement de vos donn�es, veuillez patienter...");
		loadingPlayers.add(p);
		Bukkit.getScheduler().runTaskLaterAsynchronously(FreeForAll.getInstance(), new BukkitRunnable(){

			@Override
			public void run() {
				try {
					SQLGestion.registerOnBDDFFA(p.getUniqueId().toString());
					FreeForAll.playerKills.put(p, Integer.parseInt(SQLGestion.getValue(p.getUniqueId().toString(), "kills")));
					FreeForAll.playerMorts.put(p, Integer.parseInt(SQLGestion.getValue(p.getUniqueId().toString(), "morts")));
					FreeForAll.playerRank.put(p, SQLGestion.getValue(p.getUniqueId().toString(), "rank"));
					FreeForAll.playerXP.put(p, Double.parseDouble(SQLGestion.getValue(p.getUniqueId().toString(), "xp")));
					FreeForAll.playerDC.put(p, SQLGestion.getCoins(p));
					Kits.playerKits.put(p, SQLGestion.getKits(p.getUniqueId().toString()));
					loadingPlayers.remove(p);
					scoreboardexec(p);
					p.sendMessage(GamePrefix.getGamePrefix()+"�cChargement des donn�es termin�.");
				} catch (SQLException | IOException e1) {
					e1.printStackTrace();
					p.sendMessage("�cUne Erreur[JoinEventBDD] s'est produite, merci de contacter un d�veloppeur.");
				}
			}
			
		}, 20*1 );
	}
	
	@SuppressWarnings("deprecation")
	private void scoreboardexec(final Player p){
		Bukkit.getScheduler().runTask(FreeForAll.getInstance(), new BukkitRunnable(){

			@Override
			public void run() {
				scoreboard.updateScoreboard(p);
				for(Player pls : Bukkit.getOnlinePlayers()){
					scoreboard.setteam(pls.getScoreboard(), p);
					scoreboard.setteam(p.getScoreboard(), pls);
		        }
			}
			
		});
	}

}
