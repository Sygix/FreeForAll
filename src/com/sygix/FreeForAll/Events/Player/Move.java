/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events.Player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.sygix.FreeForAll.FreeForAll;

public class Move implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		double locx = p.getLocation().getX();
		double locy = p.getLocation().getY();
		double locz = p.getLocation().getZ();
		if(FreeForAll.playerArena.containsKey(p) && FreeForAll.playerKit.containsKey(p)){ //NE PAS SORTIR DE L'ARENE
			String Arena = FreeForAll.playerArena.get(p);
			if((locx > FreeForAll.config.getInt("Arena."+Arena+".Coords.Xmin")) && (locy > FreeForAll.config.getInt("Arena."+Arena+".Coords.Ymin")) && 
					(locz > FreeForAll.config.getInt("Arena."+Arena+".Coords.Zmin"))){
				
				if((locx < FreeForAll.config.getInt("Arena."+Arena+".Coords.Xmax")) && (locy < FreeForAll.config.getInt("Arena."+Arena+".Coords.Ymax")) && 
						(locz < FreeForAll.config.getInt("Arena."+Arena+".Coords.Zmax"))){
					
				}else{
					p.sendTitle(ChatColor.DARK_RED+"Mais o� vas-tu comme �a ?", "");
					p.teleport(new Location(Bukkit.getServer().getWorld(FreeForAll.config.getString("Arena."+Arena+".World")), FreeForAll.config.getDouble("Arena."+Arena+".Spawn.X"), 
							FreeForAll.config.getDouble("Arena."+Arena+".Spawn.Y"), FreeForAll.config.getDouble("Arena."+Arena+".Spawn.Z")));
					p.playSound(p.getLocation(), Sound.ENTITY_GHAST_SCREAM, 2F, 2F);
				}
			}else{
				p.sendTitle(ChatColor.DARK_RED+"Mais o� vas-tu comme �a ?", "");
				p.teleport(new Location(Bukkit.getServer().getWorld(FreeForAll.config.getString("Arena."+Arena+".World")), FreeForAll.config.getDouble("Arena."+Arena+".Spawn.X"), 
						FreeForAll.config.getDouble("Arena."+Arena+".Spawn.Y"), FreeForAll.config.getDouble("Arena."+Arena+".Spawn.Z")));
				p.playSound(p.getLocation(), Sound.ENTITY_GHAST_SCREAM, 2F, 2F);
			}
		}else{ //NE PAS SORTIR DU SPAWN
			if((locx > FreeForAll.config.getInt("Spawn.Xmin")) && (locy > FreeForAll.config.getInt("Spawn.Ymin")) && (locz > FreeForAll.config.getInt("Spawn.Zmin"))){
				if((locx < FreeForAll.config.getInt("Spawn.Xmax")) && (locy < FreeForAll.config.getInt("Spawn.Ymax")) && (locz < FreeForAll.config.getInt("Spawn.Zmax"))){
				}else{
					p.sendTitle(ChatColor.DARK_RED+"Mais o� vas-tu comme �a ?", "");
					p.teleport(p.getWorld().getSpawnLocation());
					p.playSound(p.getLocation(), Sound.ENTITY_GHAST_SCREAM, 2F, 2F);
				}
			}else{
				p.sendTitle(ChatColor.DARK_RED+"Mais o� vas-tu comme �a ?", "");
				p.teleport(p.getWorld().getSpawnLocation());
				p.playSound(p.getLocation(), Sound.ENTITY_GHAST_SCREAM, 2F, 2F);
			}
		}
	}

}
