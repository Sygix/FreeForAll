/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Events;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import com.sygix.FreeForAll.Events.Player.BreakPickupDropPortal;
import com.sygix.FreeForAll.Events.Player.Chat;
import com.sygix.FreeForAll.Events.Player.Consume;
import com.sygix.FreeForAll.Events.Player.Damage;
import com.sygix.FreeForAll.Events.Player.Death;
import com.sygix.FreeForAll.Events.Player.Interact;
import com.sygix.FreeForAll.Events.Player.InventoryClick;
import com.sygix.FreeForAll.Events.Player.Join;
import com.sygix.FreeForAll.Events.Player.Move;
import com.sygix.FreeForAll.Events.Player.Quit;

public class EventManager {
	
	public static void registerEvent(Plugin pl){
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new Join(), pl);
		pm.registerEvents(new Quit(), pl);
		pm.registerEvents(new Move(), pl);
		pm.registerEvents(new BreakPickupDropPortal(), pl);
		pm.registerEvents(new Interact(), pl);
		pm.registerEvents(new Damage(), pl);
		pm.registerEvents(new Death(), pl);
		pm.registerEvents(new Chat(), pl);
		pm.registerEvents(new Consume(), pl);
		pm.registerEvents(new InventoryClick(), pl);
		pm.registerEvents(new Natural(), pl);
	}

}
