/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Runnables.PlayerToArena;
import com.sygix.FreeForAll.Utils.GamePrefix;

public class Leave implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(final CommandSender sender, Command cmd, String label,
			String[] args) {
		if(sender instanceof Player){
			if(FreeForAll.playerArena.containsKey(sender)){
				sender.sendMessage(GamePrefix.getGamePrefix()+"�cVous quitterez l'ar�ne dans 5 secondes...");
				Bukkit.getScheduler().runTaskLater(FreeForAll.getInstance(), new BukkitRunnable(){

					@Override
					public void run() {
						PlayerToArena.remove((Player) sender);
					}
					
				}, 20*5 );
			}else{
				sender.sendMessage(GamePrefix.getGamePrefix()+"�cVous n'�tes dans aucune ar�ne."); //Dans aucune ar�ne
			}
		}
		return false;
	}

}
