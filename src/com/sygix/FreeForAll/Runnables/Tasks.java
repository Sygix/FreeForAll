/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Runnables;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Events.Player.Join;
import com.sygix.FreeForAll.SQL.SQLGestion;
import com.sygix.FreeForAll.Utils.scoreboard;

public class Tasks {
	
	public static int scoreboardtask;
	private static boolean scoreboardrun = false;
	
	public static int dataSaveBDD;
	private static boolean dataSaveBDDrun = false;
	
	@SuppressWarnings("deprecation")
	public static void ScoreboardTask(){
		if(scoreboardrun == true){
			Bukkit.getServer().getScheduler().cancelTask(scoreboardtask);
			scoreboardrun = false;
		}else{
			scoreboardtask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(FreeForAll.getInstance(), new BukkitRunnable(){

				@Override
				public void run() {
					for(Player p : Bukkit.getServer().getOnlinePlayers()){
						if(!Join.loadingPlayers.contains(p)){
							if(FreeForAll.playerRank.get(p) != null){
								scoreboard.updateScoreboard(p);
							}
						}
					}
				}
				
			}, 20L, 20L);
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void dataSaveBDD(){
		if(dataSaveBDDrun == true){
			Bukkit.getServer().getScheduler().cancelTask(dataSaveBDD);
			dataSaveBDDrun = false;
		}else{
			dataSaveBDD = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(FreeForAll.getInstance(), new BukkitRunnable(){

				@Override
				public void run() {
					for(Player p : Bukkit.getServer().getOnlinePlayers()){
						if(!Join.loadingPlayers.contains(p)){
							if(FreeForAll.playerRank.get(p) != null){
								FreeForAll.getInstance().getLogger().info("Saving datas...");
								String uid = p.getUniqueId().toString();
								try {
									SQLGestion.setValue(uid, "kills", FreeForAll.playerKills.get(p).toString());
									SQLGestion.setValue(uid, "morts", FreeForAll.playerMorts.get(p).toString());
									SQLGestion.setValue(uid, "rank", FreeForAll.playerRank.get(p));
									SQLGestion.setValue(uid, "xp", FreeForAll.playerXP.get(p).toString());
									SQLGestion.setCoins(p, FreeForAll.playerDC.get(p));
								} catch (SQLException e) {
									e.printStackTrace();
									FreeForAll.getInstance().getLogger().info("Saving failed");
								}
								FreeForAll.getInstance().getLogger().info("Saving finished");
							}
						}
					}
				}
				
			}, 20*600, 20*600);
		}
	}

}
