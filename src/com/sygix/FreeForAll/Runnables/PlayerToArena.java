/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Runnables;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.Utils.GamePrefix;
import com.sygix.FreeForAll.Utils.InventoryManager;
import com.sygix.FreeForAll.Utils.KillStreak;

@SuppressWarnings("deprecation")
public class PlayerToArena {
	
	public static void add(Player p, String arena, String kit){
		Configuration c = FreeForAll.config;
		if(p != null && arena != null && kit != null){
			for (PotionEffect effect : p.getActivePotionEffects()){
		        p.removePotionEffect(effect.getType());
			}
			double maxlive = FreeForAll.config.getDouble("Kits."+Kits.getKitConfigName(kit)+".Max-Health");
			p.teleport(new Location(Bukkit.getServer().getWorld(c.getString("Arena."+arena+".World")), c.getInt("Arena."+arena+".Spawn.X"), c.getInt("Arena."+arena+".Spawn.Y"), c.getInt("Arena."+arena+".Spawn.Z")));
			giveKits(p, kit);
			p.setFoodLevel(19);
			p.setMaxHealth(maxlive);
			p.setHealth(maxlive);
			p.setGameMode(GameMode.ADVENTURE);
			p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(c.getDouble("Arena."+arena+".AttackSpeed"));
			p.sendMessage(GamePrefix.getGamePrefix()+"�aVous rejoignez "+arena+" ! Faites �5/leave (/l) �apour quitter.");
		}
	}
	
	public static void remove(Player p){
		FreeForAll.playerArena.remove(p);
		FreeForAll.playerKit.remove(p);
		KillStreak.resetKillStreak(p);
		p.teleport(Bukkit.getServer().getWorlds().get(0).getSpawnLocation());
		p.getInventory().clear();
		p.setFoodLevel(20);
		p.setHealth(20);
		give(p);
		for (PotionEffect effect : p.getActivePotionEffects()){
	        p.removePotionEffect(effect.getType());
		}
		p.sendMessage(GamePrefix.getGamePrefix()+"�cVous avez quitt� l'ar�ne.");
	}
	
	public static void give(Player p){
		p.getInventory().clear();
		p.getInventory().setItem(0, InventoryManager.buyKits());
		p.getInventory().setItem(4, InventoryManager.myKits());
		p.getInventory().setItem(8, InventoryManager.randomKit());
	}
	
	public static void giveKits(Player p, String kit){
		String kitconfigname = Kits.getKitConfigName(kit);
        FreeForAll.playerKit.put(p, kit);
        if (Kits.getKitsConfig().getStringList("Kits." + kitconfigname + ".Potion-Effects").size() > 0) {
          try
          {
            for (String potion : Kits.getKitsConfig().getStringList("Kits." + kitconfigname + ".Potion-Effects"))
            {
              String[] split = potion.split(" : ");
              int time;
              if ((!Kits.isNumber(split[1])) && (split[1].equalsIgnoreCase("forever"))) {
                time = 50000;
              } else {
                time = Integer.valueOf(split[1]).intValue();
              }
              p.addPotionEffect(new PotionEffect(PotionEffectType.getByName(split[0]), time, Integer.valueOf(split[2]).intValue() - 1));
            }
          }
          catch (Exception e)
          {
            p.sendMessage(ChatColor.RED + "Potion effects on this kit have been set incorrectly ! Contactez un Developpeur.");
            e.printStackTrace();
          }
        }
		p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20*15, 5));
        p.getInventory().clear();
        p.getInventory().setContents((ItemStack[])Kits.kitItems.get(kitconfigname));
        p.getInventory().setArmorContents((ItemStack[])Kits.kitArmor.get(kitconfigname));
		if(FreeForAll.config.getString("Arena."+FreeForAll.playerArena.get(p)+".Type").equalsIgnoreCase("soup")){
			p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP, 1));
		}else{
			p.getInventory().addItem(new Potion(PotionType.INSTANT_HEAL, 1).toItemStack(1));
		}
        p.updateInventory();
	}
}
