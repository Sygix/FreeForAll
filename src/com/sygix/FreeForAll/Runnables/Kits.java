/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Runnables;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.Configuration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sygix.FreeForAll.FreeForAll;
import com.sygix.FreeForAll.SQL.SQLGestion;
import com.sygix.FreeForAll.Utils.InventoryManager;

public class Kits {

	public static HashMap<String, ItemStack[]> kitItems = new HashMap<String, ItemStack[]>();
	public static HashMap<String, ItemStack[]> kitArmor = new HashMap<String, ItemStack[]>();
	public static HashMap<Player, List<String>> playerKits = new HashMap<Player, List<String>>();
	
	public static Configuration getKitsConfig(){
		return FreeForAll.config;
	}
	
	public static List<String> unlockedKits(Player p){
	    return playerKits.get(p); //Retourner une liste des kits unlocked.
	}
	
	public static boolean hasKit(Player p, String kitname){
	    try {
			return SQLGestion.checkKit(p.getUniqueId().toString(), kitname);
		} catch (SQLException e) {
			e.printStackTrace();
			p.sendMessage("�cUne Erreur[hasKits] s'est produite, merci de contacter un d�veloppeur.");
			return false;
		} //Retourner si le joueur a le kit.
	}
	
	public static ItemStack checkKit(Player p, String kitname, Material item){
		if (hasKit(p, kitname)){
	      return InventoryManager.setName(new ItemStack(item), ChatColor.GREEN + kitname);
	    }
	    return new ItemStack(Material.AIR);
	}
	
	  public static String getKitConfigName(String kit)
	  {
	    for (String kitname : getKitsConfig().getConfigurationSection("Kits").getKeys(false)) {
	      if (kit.equalsIgnoreCase(kitname)) {
	        return kitname;
	      }
	    }
	    return null;
	  }
	  
	  public static boolean isNumber(String s)
	  {
	    try
	    {
	      Integer.parseInt(s);
	      return true;
	    }
	    catch (NumberFormatException nfe) {}
	    return false;
	  }
	
	public static void addName(ItemStack s, String name)
	  {
	    try
	    {
	      ItemMeta meta = s.getItemMeta();
	      meta.setDisplayName(name.replaceAll("&", "�"));
	      s.setItemMeta(meta);
	    }
	    catch (Exception as)
	    {
	      as.printStackTrace();
	    }
	  }
	  
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void addLore(ItemStack s, String lore)
	  {
	    try
	    {
	      ItemMeta meta = s.getItemMeta();
	      List<String> lores = meta.getLore();
	      if (lores == null) {
	        lores = new ArrayList();
	      }
	      lores.add(lore.replaceAll("&", "�"));
	      meta.setLore(lores);
	      s.setItemMeta(meta);
	    }
	    catch (Exception as)
	    {
	      as.printStackTrace();
	    }
	  }
	  
	  public static void addEnchantment(ItemStack s, String name, int level){
		  try{
			  if((name.equalsIgnoreCase("unbreaking") || name.equalsIgnoreCase("durability")) && level == 255){
				  s.getItemMeta().spigot().setUnbreakable(true);
			  }
			  s.addUnsafeEnchantment(getEnchantment(name.toUpperCase()), level);
		  }catch (Exception as){
		      as.printStackTrace();
		  }
	  }
	  
	  public static Enchantment getEnchantment(String path)
	  {
	    if (path.equalsIgnoreCase("sharpness")) {
	      return Enchantment.DAMAGE_ALL;
	    }
	    if (path.equalsIgnoreCase("protection")) {
	      return Enchantment.PROTECTION_ENVIRONMENTAL;
	    }
	    if ((path.equalsIgnoreCase("blastprotection")) || (path.equalsIgnoreCase("blast_protection"))) {
	      return Enchantment.PROTECTION_EXPLOSIONS;
	    }
	    if ((path.equalsIgnoreCase("featherfalling")) || (path.equalsIgnoreCase("feather_falling"))) {
	      return Enchantment.PROTECTION_FALL;
	    }
	    if ((path.equalsIgnoreCase("fireprotection")) || (path.equalsIgnoreCase("fire_protection"))) {
	      return Enchantment.PROTECTION_FIRE;
	    }
	    if ((path.equalsIgnoreCase("projectileprotection")) || (path.equalsIgnoreCase("projectile_protection"))) {
	      return Enchantment.PROTECTION_PROJECTILE;
	    }
	    if (path.equalsIgnoreCase("power")) {
	      return Enchantment.ARROW_DAMAGE;
	    }
	    if ((path.equalsIgnoreCase("infinite")) || (path.equalsIgnoreCase("infinity"))) {
	      return Enchantment.ARROW_INFINITE;
	    }
	    if (path.equalsIgnoreCase("punch")) {
	      return Enchantment.ARROW_KNOCKBACK;
	    }
	    if (path.equalsIgnoreCase("flame")) {
	      return Enchantment.ARROW_FIRE;
	    }
	    if (path.equalsIgnoreCase("ARTHROPODS")) {
	      return Enchantment.DAMAGE_ARTHROPODS;
	    }
	    if (path.equalsIgnoreCase("smite")) {
	      return Enchantment.DAMAGE_UNDEAD;
	    }
	    if (path.equalsIgnoreCase("EFFICIENCY")) {
	      return Enchantment.DIG_SPEED;
	    }
	    if (path.equalsIgnoreCase("unbreaking")) {
	      return Enchantment.DURABILITY;
	    }
	    if ((path.equalsIgnoreCase("fireaspect")) || (path.equalsIgnoreCase("fire_aspect"))) {
	      return Enchantment.FIRE_ASPECT;
	    }
	    if (path.equalsIgnoreCase("knockback")) {
	      return Enchantment.KNOCKBACK;
	    }
	    if (path.equalsIgnoreCase("fortune")) {
	      return Enchantment.LOOT_BONUS_BLOCKS;
	    }
	    if (path.equalsIgnoreCase("looting")) {
	      return Enchantment.LOOT_BONUS_MOBS;
	    }
	    if ((path.equalsIgnoreCase("luckofthesea")) || (path.equalsIgnoreCase("luck"))) {
	      return Enchantment.LUCK;
	    }
	    if (path.equalsIgnoreCase("lure")) {
	      return Enchantment.LURE;
	    }
	    if (path.equalsIgnoreCase("OXYGEN")) {
	      return Enchantment.OXYGEN;
	    }
	    if ((path.equalsIgnoreCase("silktouch")) || (path.equalsIgnoreCase("silk_touch"))) {
	      return Enchantment.SILK_TOUCH;
	    }
	    if (path.equalsIgnoreCase("thorns")) {
	      return Enchantment.THORNS;
	    }
	    return Enchantment.getByName(path);
	  }
	
	@SuppressWarnings("deprecation")
	public static void loadKitItems(){
		FreeForAll.getInstance().getLogger().info("Loading Kits ...");
	    if (getKitsConfig().getConfigurationSection("Kits") != null) {
	    	
	      for (String kit : getKitsConfig().getConfigurationSection("Kits").getKeys(false)){ 
	        ItemStack[] items = new ItemStack[36];
	        if (getKitsConfig().getStringList("Kits." + kit + ".items").size() < 36) {
	          items = new ItemStack[getKitsConfig().getStringList("Kits." + kit + ".items").size()];
	        }
	        
	        for (int i = 0; i < items.length; i++){
	          String item = (String)getKitsConfig().getStringList("Kits." + kit + ".items").get(i);
	          String[] split = item.split(" : ");
	          ItemStack a = null;
	          try{
	            if (split[0].contains(":")){
	              a = new ItemStack(Material.getMaterial(Integer.parseInt(split[0].split(":")[0])));
	              a.setDurability((short)Integer.parseInt(split[0].split(":")[1]));
	            }else{
	              a = new ItemStack(Material.getMaterial(Integer.parseInt(split[0])), Integer.parseInt(split[1]));
	            }
	            
	          }catch (Exception e){
	            FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured creating an item in the kit " + ChatColor.GREEN + kit + ChatColor.RED + " in config.yml at the line" + ChatColor.GREEN + ": " + item);
	            e.printStackTrace();
	          }
	          
	          int s = 2;
	          while (s < split.length){
	            if ((split[s].split(":")[0].equalsIgnoreCase("ench")) || (split[s].split(":")[0].equalsIgnoreCase("name")) || (split[s].split(":")[0].equalsIgnoreCase("lore"))) {
	              try{
	                if (split[s].split(":")[0].equalsIgnoreCase("ench")) {
	                  addEnchantment(a, split[s].split(":")[1], Integer.valueOf(split[s].split(":")[2]).intValue());
	                } else if (split[s].split(":")[0].equalsIgnoreCase("name")) {
	                  addName(a, split[s].split(":")[1]);
	                } else if (split[s].split(":")[0].equalsIgnoreCase("lore")) {
	                  addLore(a, split[s].split(":")[1]);
	                }
	                
	              }catch (Exception e) {
	            	FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while adding a name/enchantment/lore to an item in config.yml for the kit " + ChatColor.GREEN + kit + ChatColor.RED + " in config.yml at the line" + ChatColor.GREEN + ": " + item);
	                e.printStackTrace();
	              }
	              
	            }
	            s++;
	          }
	          
	          try{
	            items[i] = a;
	          }
	          catch (Exception e){
	            e.printStackTrace();
	          }
	        }
	        kitItems.put(kit, items);
	      }
	    } else {
	    	FreeForAll.getInstance().getLogger().info("[FreeForAll] No kits were found in config.yml");
	    }
	    if (getKitsConfig().getConfigurationSection("Kits") != null) {
	      for (String kitname : getKitsConfig().getConfigurationSection("Kits").getKeys(false))
	      {
	        ItemStack[] armor = new ItemStack[4];
	        ItemStack helmet = null;
	        ItemStack chestplate = null;
	        ItemStack leggings = null;
	        ItemStack boots = null;
	        Configuration c = getKitsConfig();
	        try
	        {
	          if (c.getString("Kits." + kitname + ".Armor.Helmet").split(" : ")[0].contains(":"))
	          {
	            helmet = new ItemStack(Material.getMaterial(Integer.valueOf(c.getString("Kits." + kitname + ".Armor.Helmet").split(" : ")[0].split(":")[0].replaceAll(" ", "")).intValue()));
	            int durability = Integer.parseInt(c.getString("Kits." + kitname + ".Armor.Helmet").split(" : ")[0].split(":")[1].replaceAll(" ", ""));
	            helmet.setDurability((short)durability);
	          }
	          else
	          {
	            helmet = new ItemStack(Material.getMaterial(Integer.valueOf(c.getString("Kits." + kitname + ".Armor.Helmet").split(" : ")[0]).intValue()));
	          }
	        }
	        catch (Exception e)
	        {
	        	FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while creating the helmet of the kit: " + ChatColor.GREEN + kitname + ChatColor.RED + " in config.yml");
	          e.printStackTrace();
	        }
	        try
	        {
	          if (c.getString("Kits." + kitname + ".Armor.Chestplate").split(" : ")[0].contains(":"))
	          {
	            chestplate = new ItemStack(Material.getMaterial(Integer.valueOf(c.getString("Kits." + kitname + ".Armor.Chestplate").split(" : ")[0].split(":")[0].replaceAll(" ", "")).intValue()));
	            int durability = Integer.parseInt(c.getString("Kits." + kitname + ".Armor.Chestplate").split(" : ")[0].split(":")[1].replaceAll(" ", ""));
	            chestplate.setDurability((short)durability);
	          }
	          else
	          {
	            chestplate = new ItemStack(Material.getMaterial(Integer.valueOf(c.getString("Kits." + kitname + ".Armor.Chestplate").split(" : ")[0]).intValue()));
	          }
	        }
	        catch (Exception e)
	        {
	        	FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while creating the chestplate of the kit: " + ChatColor.GREEN + kitname + ChatColor.RED + " in kits.yml");
	          e.printStackTrace();
	        }
	        try
	        {
	          if (c.getString("Kits." + kitname + ".Armor.Leggings").split(" : ")[0].contains(":"))
	          {
	            leggings = new ItemStack(Material.getMaterial(Integer.valueOf(c.getString("Kits." + kitname + ".Armor.Leggings").split(" : ")[0].split(":")[0].replaceAll(" ", "")).intValue()));
	            int durability = Integer.parseInt(c.getString("Kits." + kitname + ".Armor.Leggings").split(" : ")[0].split(":")[1].replaceAll(" ", ""));
	            leggings.setDurability((short)durability);
	          }
	          else
	          {
	            leggings = new ItemStack(Material.getMaterial(Integer.valueOf(c.getString("Kits." + kitname + ".Armor.Leggings").split(" : ")[0]).intValue()));
	          }
	        }
	        catch (Exception e)
	        {
	        	FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while creating the leggings of the kit: " + ChatColor.GREEN + kitname + ChatColor.RED + " in kits.yml");
	          e.printStackTrace();
	        }
	        try
	        {
	          if (c.getString("Kits." + kitname + ".Armor.Boots").split(" : ")[0].contains(":"))
	          {
	            boots = new ItemStack(Material.getMaterial(Integer.valueOf(c.getString("Kits." + kitname + ".Armor.Boots").split(" : ")[0].split(":")[0].replaceAll(" ", "")).intValue()));
	            int durability = Integer.parseInt(c.getString("Kits." + kitname + ".Armor.Boots").split(" : ")[0].split(":")[1].replaceAll(" ", ""));
	            boots.setDurability((short)durability);
	          }
	          else
	          {
	            boots = new ItemStack(Material.getMaterial(Integer.valueOf(c.getString("Kits." + kitname + ".Armor.Boots").split(" : ")[0]).intValue()));
	          }
	        }
	        catch (Exception e)
	        {
	        	FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while creating the boots of the kit: " + ChatColor.GREEN + kitname + ChatColor.RED + " in kits.yml");
	          e.printStackTrace();
	        }
	        if ((c.getString("Kits." + kitname + ".Armor.Helmet").contains(" : ")) && (helmet != null)) {
	          try
	          {
	            String[] splits = c.getString("Kits." + kitname + ".Armor.Helmet").split(" : ");
	            int s = 1;
	            while (s < splits.length)
	            {
	              if ((splits[s].split(":")[0].equalsIgnoreCase("ench")) || (splits[s].split(":")[0].equalsIgnoreCase("name")) || (splits[s].split(":")[0].equalsIgnoreCase("lore"))) {
	                if (splits[s].split(":")[0].equalsIgnoreCase("ench")) {
	                  addEnchantment(helmet, splits[s].split(":")[1], Integer.valueOf(splits[s].split(":")[2]).intValue());
	                } else if (splits[s].split(":")[0].equalsIgnoreCase("name")) {
	                  addName(helmet, splits[s].split(":")[1]);
	                } else if (splits[s].split(":")[0].equalsIgnoreCase("lore")) {
	                  addLore(helmet, splits[s].split(":")[1]);
	                }
	              }
	              s++;
	            }
	          }
	          catch (Exception e)
	          {
	        	  FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while adding a enchant/name/lore to the helmet of the kit: " + ChatColor.GREEN + kitname + ChatColor.RED + " in kits.yml");
	            e.printStackTrace();
	          }
	        }
	        if ((c.getString("Kits." + kitname + ".Armor.Chestplate").contains(" : ")) && (chestplate != null)) {
	          try
	          {
	            String[] splits = c.getString("Kits." + kitname + ".Armor.Chestplate").split(" : ");
	            int s = 1;
	            while (s < splits.length)
	            {
	              if ((splits[s].split(":")[0].equalsIgnoreCase("ench")) || (splits[s].split(":")[0].equalsIgnoreCase("name")) || (splits[s].split(":")[0].equalsIgnoreCase("lore"))) {
	                if (splits[s].split(":")[0].equalsIgnoreCase("ench")) {
	                  addEnchantment(chestplate, splits[s].split(":")[1], Integer.valueOf(splits[s].split(":")[2]).intValue());
	                } else if (splits[s].split(":")[0].equalsIgnoreCase("name")) {
	                  addName(chestplate, splits[s].split(":")[1]);
	                } else if (splits[s].split(":")[0].equalsIgnoreCase("lore")) {
	                  addLore(chestplate, splits[s].split(":")[1]);
	                }
	              }
	              s++;
	            }
	          }
	          catch (Exception e)
	          {
	        	  FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while adding a enchant/name/lore to the chestplate of the kit: " + ChatColor.GREEN + kitname + ChatColor.RED + " in kits.yml");
	            e.printStackTrace();
	          }
	        }
	        if ((c.getString("Kits." + kitname + ".Armor.Leggings").contains(" : ")) && (leggings != null)) {
	          try
	          {
	            String[] splits = c.getString("Kits." + kitname + ".Armor.Leggings").split(" : ");
	            int s = 1;
	            while (s < splits.length)
	            {
	              if ((splits[s].split(":")[0].equalsIgnoreCase("ench")) || (splits[s].split(":")[0].equalsIgnoreCase("name")) || (splits[s].split(":")[0].equalsIgnoreCase("lore"))) {
	                if (splits[s].split(":")[0].equalsIgnoreCase("ench")) {
	                  addEnchantment(leggings, splits[s].split(":")[1], Integer.valueOf(splits[s].split(":")[2]).intValue());
	                } else if (splits[s].split(":")[0].equalsIgnoreCase("name")) {
	                  addName(leggings, splits[s].split(":")[1]);
	                } else if (splits[s].split(":")[0].equalsIgnoreCase("lore")) {
	                  addLore(leggings, splits[s].split(":")[1]);
	                }
	              }
	              s++;
	            }
	          }
	          catch (Exception e)
	          {
	        	  FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while adding a enchant/name/lore to the leggings of the kit: " + ChatColor.GREEN + kitname + ChatColor.RED + " in kits.yml");
	            e.printStackTrace();
	          }
	        }
	        if ((c.getString("Kits." + kitname + ".Armor.Boots").contains(" : ")) && (boots != null)) {
	          try
	          {
	            String[] splits = c.getString("Kits." + kitname + ".Armor.Boots").split(" : ");
	            int s = 1;
	            while (s < splits.length)
	            {
	              if ((splits[s].split(":")[0].equalsIgnoreCase("ench")) || (splits[s].split(":")[0].equalsIgnoreCase("name")) || (splits[s].split(":")[0].equalsIgnoreCase("lore"))) {
	                if (splits[s].split(":")[0].equalsIgnoreCase("ench")) {
	                  addEnchantment(boots, splits[s].split(":")[1], Integer.valueOf(splits[s].split(":")[2]).intValue());
	                } else if (splits[s].split(":")[0].equalsIgnoreCase("name")) {
	                  addName(boots, splits[s].split(":")[1]);
	                } else if (splits[s].split(":")[0].equalsIgnoreCase("lore")) {
	                  addLore(boots, splits[s].split(":")[1]);
	                }
	              }
	              s++;
	            }
	          }
	          catch (Exception e)
	          {
	        	  FreeForAll.getInstance().getLogger().info("[FreeForAll] " + ChatColor.RED + "an Error has occured while adding a enchant/name/lore to the boots of the kit: " + ChatColor.GREEN + kitname + ChatColor.RED + " in kits.yml");
	            e.printStackTrace();
	          }
	        }
	        armor[3] = helmet;
	        armor[2] = chestplate;
	        armor[1] = leggings;
	        armor[0] = boots;
	        kitArmor.put(kitname, armor);
	      }
	    }
	    FreeForAll.getInstance().getLogger().info("Finished loading kits!");
	  }

}
