/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Utils;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sygix.FreeForAll.Runnables.Kits;

public class InventoryManager {
	
	//Items
	
	public static ItemStack setName(ItemStack is, String name){
	    ItemMeta m = is.getItemMeta();
	    m.setDisplayName(name);
	    is.setItemMeta(m);
	    return is;
	}
	
	public static ItemStack buyKits(){
		ItemStack item = new ItemStack(Material.ENDER_CHEST, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+""+ChatColor.BOLD+"Boutique de kits "+ChatColor.GRAY+"(Clique droit)");
	    item.setItemMeta(itemm);
	    return item;
	}
	public static ItemStack myKits(){
		ItemStack item = new ItemStack(Material.CHEST, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+""+ChatColor.BOLD+"Mes kits "+ChatColor.GRAY+"(Clique droit)");
	    item.setItemMeta(itemm);
	    return item;
	}
	public static ItemStack randomKit(){
		ItemStack item = new ItemStack(Material.NETHER_STAR, 1);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.GOLD+""+ChatColor.BOLD+"Acheter un kit random "+ChatColor.GRAY+"(Clique droit)");
	    item.setItemMeta(itemm);
	    return item;
	}
	
	//Menus
	
	public static Inventory buyKitsMenu(){
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.DARK_RED+""+ChatColor.BOLD+"Ach�te un Kit");
		
		return inv;
	}
	
	public static Inventory myKitsMenu(){
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.DARK_RED+""+ChatColor.BOLD+"Mes Kits");
		
		return inv;
	}
	
	public static Inventory randomKitMenu(){
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.DARK_RED+""+ChatColor.BOLD+"Ach�te un Kit RANDOM");
		
		return inv;
	}
	
	@SuppressWarnings("deprecation")
	public static void selectKitMenu(Player p){
		String kitconfigname;
		Inventory inv = Bukkit.createInventory(null , 54, ChatColor.DARK_RED+""+ChatColor.BOLD+"S�lectionne ton Kit");
		List<String> kitss = Kits.unlockedKits(p);
        for (int i = 0; i < kitss.size(); i++){
          kitconfigname = (String)kitss.get(i);
          if (Kits.getKitsConfig().contains("Kits." + kitconfigname)) {
            inv.addItem(new ItemStack[] { Kits.checkKit(p, kitconfigname, Material.getMaterial(Kits.getKitsConfig().getInt("Kits." + kitconfigname + ".Item"))) });
          }
        }
        p.openInventory(inv);
	}
	
}
