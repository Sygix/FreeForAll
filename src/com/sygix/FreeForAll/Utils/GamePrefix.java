/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Utils;

import org.bukkit.ChatColor;

public class GamePrefix {
	
	static String prefix = ChatColor.GOLD+""+ChatColor.BOLD+"FreeForAll "+ChatColor.GOLD+": "+ChatColor.RESET;
	
	public static String getGamePrefix(){
		return prefix;
	}

}
