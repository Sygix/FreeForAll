/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Utils;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.sygix.FreeForAll.FreeForAll;

public class KillStreak {
	
	public static HashMap<Player, Integer> killstreak = new HashMap<Player, Integer>();
	private static HashMap<Player, Integer> multiplierXP = new HashMap<Player, Integer>();
	
	public static void addKillStreak(Player p){
		if(killstreak.containsKey(p)){
			int ks = killstreak.get(p);
			killstreak.put(p, killstreak.get(p)+1);
			if(ks == 10 || ks == 20 || ks == 30 || ks == 40 || ks == 50 || ks == 60 || ks == 70 || ks == 80 || ks == 90 || ks == 100){
				multiplierXP.put(p, multiplierXP.get(p)+1);
			}
			FreeForAll.playerDC.replace(p, FreeForAll.playerDC.get(p)+1);
			FreeForAll.playerXP.replace(p, FreeForAll.playerXP.get(p)+1*multiplierXP.get(p));
			
			p.sendMessage(GamePrefix.getGamePrefix()+"�eVous venez de faire 1 kill !"+
					"\n    - XP : �b+"+ (1*multiplierXP.get(p))+
					"\n�e    - DirektCoins : �b+1");
		}else{			
			FreeForAll.playerDC.replace(p, FreeForAll.playerDC.get(p)+1);
			FreeForAll.playerXP.replace(p, FreeForAll.playerXP.get(p)+1);
			
			p.sendMessage(GamePrefix.getGamePrefix()+"�eVous venez de faire 1 kill !"+
					"\n    - XP : �b+1"+
					"\n�e    - DirektCoins : �b+1");

			killstreak.put(p, 1);
			multiplierXP.put(p, 1);
		}
		if(FreeForAll.config.getInt("Rank."+Rank.getNextRank(p)+".xp") - FreeForAll.playerXP.get(p) <= 0){
			BroadcastMessage.broadcast(GamePrefix.getGamePrefix()+"�3"+p.getDisplayName()+" a pass� un niveau ! "+ChatColor.translateAlternateColorCodes('&', FreeForAll.config.getString("Rank."+FreeForAll.playerRank.get(p)+".Color"))+FreeForAll.playerRank.get(p)+"�3 > "+ChatColor.translateAlternateColorCodes('&', FreeForAll.config.getString("Rank."+Rank.getNextRank(p)+".Color"))+Rank.getNextRank(p));
			FreeForAll.playerXP.replace(p, 0.0);
			FreeForAll.playerRank.replace(p, Rank.getNextRank(p));
		}
	}
	
	public static void resetKillStreak(Player p){
		killstreak.remove(p);
		multiplierXP.remove(p);
	}

}
