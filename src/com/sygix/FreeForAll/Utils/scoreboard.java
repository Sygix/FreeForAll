/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.sygix.FreeForAll.FreeForAll;

@SuppressWarnings("deprecation")
public class scoreboard {
	
	private static Team admin, assist, respdev, respmodo, resparchi, developpeur, staff, moderateur, architecte, helper, videaste, friend, donateur, vip, minivip, joueur;
	
	public static void setupTeam(Scoreboard sb){
		admin = sb.registerNewTeam("A");
		admin.setPrefix("§4[Admin] ");
		admin.setSuffix(" Ⓢ");
		admin.setNameTagVisibility(NameTagVisibility.ALWAYS);
		assist = sb.registerNewTeam("B");
		assist.setPrefix("§4[A. Admin] ");
		assist.setSuffix(" Ⓢ");
		assist.setNameTagVisibility(NameTagVisibility.ALWAYS);
		respdev = sb.registerNewTeam("C");
		respdev.setPrefix("§c[Resp. Dév] ");
		respdev.setSuffix(" Ⓢ");
		respdev.setNameTagVisibility(NameTagVisibility.ALWAYS);
		respmodo = sb.registerNewTeam("D");
		respmodo.setPrefix("§9[Resp. Modo] ");
		respmodo.setSuffix(" Ⓢ");
		respmodo.setNameTagVisibility(NameTagVisibility.ALWAYS);
		resparchi = sb.registerNewTeam("E");
		resparchi.setPrefix("§5[Resp. Archi] ");
		resparchi.setSuffix(" Ⓢ");
		resparchi.setNameTagVisibility(NameTagVisibility.ALWAYS);
		developpeur = sb.registerNewTeam("F");
		developpeur.setPrefix("§c[Développeur] ");
		developpeur.setSuffix(" Ⓢ");
		developpeur.setNameTagVisibility(NameTagVisibility.ALWAYS);
		staff = sb.registerNewTeam("G");
		staff.setPrefix("§2[Staff] ");
		staff.setSuffix(" Ⓢ");
		staff.setNameTagVisibility(NameTagVisibility.ALWAYS);
		moderateur = sb.registerNewTeam("H");
		moderateur.setPrefix("§9[Modérateur] ");
		moderateur.setSuffix(" Ⓢ");
		moderateur.setNameTagVisibility(NameTagVisibility.ALWAYS);
		architecte = sb.registerNewTeam("I");
		architecte.setPrefix("§5[Architecte] ");
		architecte.setSuffix(" Ⓢ");
		architecte.setNameTagVisibility(NameTagVisibility.ALWAYS);
		helper = sb.registerNewTeam("J");
		helper.setPrefix("§3[Helper] ");
		helper.setSuffix(" Ⓢ");
		helper.setNameTagVisibility(NameTagVisibility.ALWAYS);
		videaste = sb.registerNewTeam("K");
		videaste.setPrefix("§6[Vidéaste] ");
		videaste.setNameTagVisibility(NameTagVisibility.ALWAYS);
		friend = sb.registerNewTeam("L");
		friend.setPrefix("§5[D. Friend] ");
		friend.setNameTagVisibility(NameTagVisibility.ALWAYS);
		donateur = sb.registerNewTeam("M");
		donateur.setPrefix("§3[Donateur] ");
		donateur.setNameTagVisibility(NameTagVisibility.ALWAYS);
		vip = sb.registerNewTeam("N");
		vip.setPrefix("§a[VIP] ");
		vip.setNameTagVisibility(NameTagVisibility.ALWAYS);
		minivip = sb.registerNewTeam("O");
		minivip.setPrefix("§2[Mini-VIP] ");
		minivip.setNameTagVisibility(NameTagVisibility.ALWAYS);
		joueur = sb.registerNewTeam("Z");
		joueur.setPrefix("§7");
		joueur.setNameTagVisibility(NameTagVisibility.ALWAYS);
	}
	
	public static void setteam(Scoreboard sb, Player p){
		if(p.hasPermission("DirektServ.admin")){
			sb.getTeam(admin.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.assist")){
			sb.getTeam(assist.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.respdev")){
			sb.getTeam(respdev.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.respmodo")){
			sb.getTeam(respmodo.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.resparchi")){
			sb.getTeam(resparchi.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.developpeur")){
			sb.getTeam(developpeur.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.staff")){
			sb.getTeam(staff.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.moderateur")){
			sb.getTeam(moderateur.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.architecte")){
			sb.getTeam(architecte.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.helper")){
			sb.getTeam(helper.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.videaste")){
			sb.getTeam(videaste.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.friend")){
			sb.getTeam(friend.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.donateur")){
			sb.getTeam(donateur.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.vip")){
			sb.getTeam(vip.getName()).addPlayer(p);
		}else if(p.hasPermission("DirektServ.minivip")){
			sb.getTeam(minivip.getName()).addPlayer(p);
		}else{
			sb.getTeam(joueur.getName()).addPlayer(p);
		}
	}
	
	public static void updateScoreboard(Player p){
		
		if ((p.getScoreboard().getObjective(DisplaySlot.SIDEBAR) == null) || (!p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).getDisplayName().equalsIgnoreCase("§6§lFreeForAll")))
	      {
	        Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
	        Objective objective = board.registerNewObjective("FreeForAll", "dummy");
	        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
	        objective.setDisplayName("§6§lFreeForAll");
	        objective.getScore("").setScore(-1);
	        objective.getScore("§7---------------").setScore(-2);
	        objective.getScore(" ").setScore(-3);
	        objective.getScore("§3play.direktserv.fr").setScore(-4);
	        updateScoreboardContent(objective, p);
	        setupTeam(board);
			scoreboard.setteam(board, p);
	        p.setScoreboard(board);
	      }
	      else
	      {
	        Objective objective = p.getScoreboard().getObjective(DisplaySlot.SIDEBAR);
	        updateScoreboardContent(objective, p);
	      }
	    
	  }
	  
	  public static void updateScoreboardContent(Objective objective, Player p){
		  
	    Score DC = objective.getScore(ChatColor.GREEN + "DirektCoins :");
	    Score Death = objective.getScore(ChatColor.GREEN + "Morts :");
	    Score Kills = objective.getScore(ChatColor.GREEN + "Kills :");
	    Score Killstreak = objective.getScore(ChatColor.GREEN + "Killstreak :");
	    Score Exp = objective.getScore(ChatColor.GREEN + "Niveau suivant :");
	    
	    Double dDC = FreeForAll.playerDC.get(p);
	    Double nextlvl = FreeForAll.config.getInt("Rank."+Rank.getNextRank(p)+".xp") - (FreeForAll.playerXP.get(p));
	    
	    DC.setScore(dDC.intValue());
	    Kills.setScore(FreeForAll.playerKills.get(p));
	    Death.setScore(FreeForAll.playerMorts.get(p));
	    if(KillStreak.killstreak.get(p) != null){
		    Killstreak.setScore(KillStreak.killstreak.get(p));
	    }else{
		    Killstreak.setScore(0);
	    }
	    Exp.setScore(nextlvl.intValue());
	 }

}
