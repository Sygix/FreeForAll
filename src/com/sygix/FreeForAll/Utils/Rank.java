/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll.Utils;

import org.bukkit.entity.Player;

import com.sygix.FreeForAll.FreeForAll;

public class Rank {
	
	public static void setupRanks(){
		FreeForAll.getInstance().getLogger().info("Chargement des ranks...");
		FreeForAll.ranks.addAll(FreeForAll.config.getConfigurationSection("Rank").getKeys(false));
		if(FreeForAll.ranks.isEmpty()){
			FreeForAll.getInstance().getLogger().info("Chargement des ranks �chou�");
			return;
		}
		FreeForAll.getInstance().getLogger().info("Chargement des ranks termin�");
	}
	
	public static String getNextRank(Player p)
	  {
	    try
	    {
	      String nextRank = "";
	      int target = 0;
	      for (int i = 0; i < FreeForAll.ranks.size(); i++) {
	        if (((String)FreeForAll.ranks.get(i)).equalsIgnoreCase(FreeForAll.playerRank.get(p)))
	        {
	          target = i + 1;
	          if (target < FreeForAll.ranks.size())
	          {
	            nextRank = (String)FreeForAll.ranks.get(target);
	            break;
	          }
	          nextRank = "";
	          
	          break;
	        }
	      }
	      return nextRank;
	    }
	    catch (Exception e) {}
	    return "NORANK";
	  }

}
