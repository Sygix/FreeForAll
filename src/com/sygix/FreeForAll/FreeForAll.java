/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.FreeForAll;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.sygix.FreeForAll.Commands.Leave;
import com.sygix.FreeForAll.Events.EventManager;
import com.sygix.FreeForAll.Runnables.Kits;
import com.sygix.FreeForAll.Runnables.Tasks;
import com.sygix.FreeForAll.SQL.SQLGestion;
import com.sygix.FreeForAll.Utils.Rank;

public class FreeForAll extends JavaPlugin{
	
	public static FreeForAll instance;
	public static Configuration config;
	public static HashMap<Player, String> playerArena = new HashMap<Player, String>();
	public static HashMap<Player, String> playerKit = new HashMap<Player, String>();
	public static HashMap<Player, Integer> playerKills = new HashMap<Player, Integer>();
	public static HashMap<Player, Integer> playerMorts = new HashMap<Player, Integer>();
	public static HashMap<Player, String> playerRank = new HashMap<Player, String>(); //Toujour remplir celui-ci, /!\ Permet de check si null
	public static HashMap<Player, Double> playerXP = new HashMap<Player, Double>();
	public static HashMap<Player, Double> playerDC = new HashMap<Player, Double>();
	public static ArrayList<String> ranks = new ArrayList<String>();
	
	public static FreeForAll getInstance(){
		return instance;
	}
	
	public void onEnable(){
		getLogger().info("Chargement ...");
		if(!this.getDataFolder().exists()){
			this.getDataFolder().mkdir();
		}
		this.saveDefaultConfig();
		config = this.getConfig();
		instance = this;
		for(String arena : config.getConfigurationSection("Arena").getKeys(false)){
			getServer().createWorld(new WorldCreator(config.getString("Arena."+arena+".World")));
		}
		for(World w : Bukkit.getWorlds()){
			w.setAutoSave(false);
			w.setGameRuleValue("randomTickSpeed", "0");
			w.setGameRuleValue("doEntityDrop", "false");
			w.setGameRuleValue("doMobSpawning", "false");
			w.setGameRuleValue("doFireTick", "false");
			w.setGameRuleValue("doMobLoot", "false");
			w.setGameRuleValue("mobGriefing", "false");
			w.setGameRuleValue("spectatorsGenerateChunks", "false");
		}
		try {
			SQLGestion.connect();
			SQLGestion.registerPlugin();
			SQLGestion.registerServerOnBDD(Bukkit.getServerName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		try{
			SQLGestion.setState(Bukkit.getServerName(), "OPEN");
		}catch(Exception e){
			e.printStackTrace();
		}
		Kits.loadKitItems();
		Rank.setupRanks();
		Tasks.ScoreboardTask();
		Tasks.dataSaveBDD();
		EventManager.registerEvent(this);
		getCommand("leave").setExecutor(new Leave());
		getLogger().info("Chargement termine !");
	}
	
	public void onDisble(){
		getLogger().info("Dechargement ...");
		Bukkit.getScheduler().cancelTasks(this);
		try {
			SQLGestion.setState(Bukkit.getServerName(), "DOWN");
			SQLGestion.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		getLogger().info("Dechargement termine !");
	}

}
